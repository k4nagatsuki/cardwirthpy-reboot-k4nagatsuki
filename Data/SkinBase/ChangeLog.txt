SkinBase 変更履歴
=================

2019年7月9日(dataVersion=12)
----------------------------
 * 既存のクラシックエンジンからTable/Midi/Waveのリソースをそのまま持ってきているかを示すパラメータ`Property/SourceOfMaterialsIsClassicEngine`を追加。

2019年6月8日(dataVersion=11)
----------------------------
 * `Resource/Image/Other/SCREENSHOT_HEADER.png`と`Resource/Image/Other/SCREENSHOT_HEADER.x2.png`を追加。

2019年5月11日(dataVersion=11)
----------------------------
 * 標準型`SampleType`の解説を追加。

2016年10月23日(dataVersion=11)
----------------------------
 * シナリオ選択ダイアログの後に遷移する宿エリア`Resource/Xml/Yado/04_StartScenario.xml`を追加。
   このバージョン以降、スキンに無いエリア定義は`SkinBase`のものが使用されるため、`dataVersion=10`以前のスキンのアップデートは不要。

2016年3月27日(dataVersion=10)
----------------------------
 * オープニングアニメーション定義`Resource/Xml/Animation/Opening.xml`を追加。

2016年3月5日(dataVersion=9)
----------------------------
 * タイトル画面のカード位置を調節。
 * 初期状態の宿で表示される`Resource/Xml/Yado/03_YadoInitial.xml`を追加。

2016年1月28日(dataVersion=8)
----------------------------
 * 初期資金の要素を追加。

2015年6月15日(dataVersion=7)
----------------------------
 * 混乱カードの効果をCardWirthに合わせた。

2014年8月2日(dataVersion=6)
----------------------------
 * PostEventコンテントのcommandとargをMenuCardに直接設定できるようにした。

2014年6月28日(dataVersion=5)
----------------------------
 * パーティ編成記録関係のメッセージを追加。
 * シナリオ絞込条件関係のメッセージを追加。
 * シナリオのブックマーク関係のメッセージを追加。
 * その他インタフェース関係のメッセージを追加。

2014年5月31日(dataVersion=4)
----------------------------
 * Resource/Xml/SpecialCard/UseCardInBackpack.xmlを追加。

2014年4月21日(dataVersion=3)
---------------------------
 * ゲームオーバーの画面処理を修正
 * フェイントの発動音を修正

2014年3月3日(dataVersion=2)
---------------------------

 * SampleType以外で、各特性の精神修正値の慎重-大胆(cautious)と社交-内向(cheerful)の値が入れ替わっていたのを修正
 * SampleTypeで精神修正値の値が1/2になっていたのを修正
